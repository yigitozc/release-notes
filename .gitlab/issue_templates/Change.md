This issue tracks the release note for the following Fedora Change:

!!!!LINK TO WIKI HERE!!!!

**If you own this change**, please add additional information here that we should communicate to Fedora users.  Specifically, please consider:

* New features available because of this change - pick those that are important to end users
* Considerations for users of previous releases of Fedora (upgrade issues, format changes, etc.)
* Links to any upstream Release Notes
* If this helps Fedora be a superior environment for our target audiences, please explain how so that we can emphasize this.

Your notes to us do not need to be formally written - we will edit them and add details if needed.  This is a way for you to ensure that we know what is critical about your change.

**If you want to write this release note**, then:

* Assign this issue to yourself
* Check the wiki page linked above, find out what the change is about
* Determine whether the change actually made it into the release or not[0]
* Write a draft release note using that information against the correct branch here, in Gitlab. (or see below)
* Get in touch with the contact person/people listed on the wiki page, either through IRC/Matrix or e-mail or just by @-mentioning them in a comment here, and ask them to check your draft for technical accuracy
* Submit your Release Note as a PR to this repository.

Once you're done with the above, make sure to either commit the relnote to an appropriate section of the Release Notes, or, if you're not familiar with Git, AsciiDoc, or whatever else, just add it to this issue as a comment and let the team[1] know that you're done with this one and you'd like the note included. Be sure to do this at least one day before the final release (see current schedule[2]). Also make sure to do this even for relnotes that haven't been checked by the change owner.

[0] You can do that by asking the change owner listed on the wiki page; alternatively you can infer it by checking the tracker bug (linked in Wiki) in Bugzilla and looking at its status; see bug comments for details. Ask someone on Matrix if you're not sure.  
[1] On Matrix in the Fedora Documentation channel - see https://docs.fedoraproject.org/en-US/project/communications/.  
[2] Schedules are all here: https://fedorapeople.org/groups/schedule/ - make sure to pick the correct release.  
