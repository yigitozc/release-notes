include::partial$entities.adoc[]

[[sect-welcome_to_fedora]]
= Welcome to Fedora

The Fedora Project is a partnership of free software community members from around the globe. The Fedora Project builds open source software communities and produces a Linux distribution called Fedora.

The Fedora Project's mission is to lead the advancement of free and open source software and content as a collaborative community. The three elements of this mission are clear:

* The Fedora Project always strives to lead, not follow.

* The Fedora Project consistently seeks to create, improve, and spread free/libre code and content.

* The Fedora Project succeeds through shared action on the part of many people throughout our community.

To find out more general information about Fedora, refer to the following pages, on the Fedora Project Wiki:

* link:++https://fedoraproject.org/wiki/Overview++[Fedora Overview]

* link:++https://fedoraproject.org/wiki/FAQ++[Fedora FAQ]

* link:++https://fedoraproject.org/wiki/Communicate++[Help and Discussions]

* link:++https://fedoraproject.org/wiki/Join++[Participate in the Fedora Project]

[[sect-need-help]]
== Need Help?

There are a number of places you can get assistance should you run into problems.

If you run into a problem and would like some assistance, go to link:++https://ask.fedoraproject.org++[]. Many answers are already there, but if you don't find yours, you can simply post a new question. This has the advantage that anyone else with the same problem can find the answer, too.

You may also find assistance on the `#fedora` channel on the IRC network `irc.libera.chat`. Keep in mind that the channel is populated by volunteers wanting to help, but folks knowledgeable about a specific topic might not always be available.

[[sect-providing-help]]
== Want to Contribute?

You can help the Fedora Project community continue to improve Fedora if you file bug reports and enhancement requests. Refer to link:++https://fedoraproject.org/wiki/BugsAndFeatureRequests++[Bugs And Feature Requests] on the Fedora Wiki for more information about bug and feature reporting. Thank you for your participation.
